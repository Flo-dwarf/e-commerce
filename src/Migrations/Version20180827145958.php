<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180827145958 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_category (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_CDFC73564584665A (product_id), INDEX IDX_CDFC735612469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opinion (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, product_id INT NOT NULL, date DATE NOT NULL, star_evaluation INT NOT NULL, content VARCHAR(255) NOT NULL, INDEX IDX_AB02B027F675F31B (author_id), INDEX IDX_AB02B0274584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC73564584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC735612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B027F675F31B FOREIGN KEY (author_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B0274584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product DROP category, DROP reference');
        $this->addSql('ALTER TABLE line_shopping_cart ADD shopping_cart_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE line_shopping_cart ADD CONSTRAINT FK_EA41C09F45F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id)');
        $this->addSql('CREATE INDEX IDX_EA41C09F45F80CD ON line_shopping_cart (shopping_cart_id)');
        $this->addSql('ALTER TABLE user CHANGE street street VARCHAR(255) NOT NULL, CHANGE zip_code zip_code VARCHAR(255) NOT NULL, CHANGE city city VARCHAR(255) NOT NULL, CHANGE country country VARCHAR(255) NOT NULL, CHANGE admin admin TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE opinion');
        $this->addSql('ALTER TABLE line_shopping_cart DROP FOREIGN KEY FK_EA41C09F45F80CD');
        $this->addSql('DROP INDEX IDX_EA41C09F45F80CD ON line_shopping_cart');
        $this->addSql('ALTER TABLE line_shopping_cart DROP shopping_cart_id');
        $this->addSql('ALTER TABLE product ADD category VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD reference INT NOT NULL');
        $this->addSql('ALTER TABLE `user` CHANGE street street VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE zip_code zip_code VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE country country VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE admin admin TINYINT(1) DEFAULT NULL');
    }
}
