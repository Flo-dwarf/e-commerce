<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180828124626 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE line_shopping_cart ADD shopping_cart_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE line_shopping_cart ADD CONSTRAINT FK_EA41C09F45F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id)');
        $this->addSql('CREATE INDEX IDX_EA41C09F45F80CD ON line_shopping_cart (shopping_cart_id)');
        $this->addSql('ALTER TABLE category DROP product');
        $this->addSql('ALTER TABLE user CHANGE admin admin TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category ADD product INT NOT NULL');
        $this->addSql('ALTER TABLE line_shopping_cart DROP FOREIGN KEY FK_EA41C09F45F80CD');
        $this->addSql('DROP INDEX IDX_EA41C09F45F80CD ON line_shopping_cart');
        $this->addSql('ALTER TABLE line_shopping_cart DROP shopping_cart_id');
        $this->addSql('ALTER TABLE `user` CHANGE admin admin VARCHAR(45) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
