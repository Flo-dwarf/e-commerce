<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\LineShoppingCart;
use App\Repository\ProductRepository;
use App\Entity\ShoppingCart;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class LineShoppingCartController extends Controller
{
    /**
     * @Route("user/{id}/{number}/line_shopping_cart", name="line_shopping_cart")
     */
    public function index(Request $request, UserInterface $user, int $id, int $quantity, ProductRepository $repository,   EntityManagerInterface $entityManager)
    {   
        $entityManager = $this->getDoctrine()->getManager();
        
        if (!$shoppingCart) {//si le panier n'existe pas encore car vide
            $shoppingCart = new ShoppingCart();//crée une nouvelle instance de ShoppingCart
        } else {
            $shoppingCart = $entityManager->merge($shoppingCart);//la méthode merge modifie le panier existant et je mets cette modif dans la variable shoppingCart
        }


        $lineShoppingCart = new LineShoppingCart(); //je crée une nouvelle instance de la classe/entité LineShoppingCart
        $repository = $repository->find($id);//je récupère par l'id le produit qui est dans la base de données en passant par le repository et je l'assigne à la variable $repository

        $lineShoppingCart->setProduct($repository);//je fixe dans la variable lineShoppingCart le produit que j'ai récupéré dans la variable $repository 
        $lineShoppingCart->setQuantity($quantity);//je fixe dans la variable lineShoppingCart la quantité de ce produit

        $price = $repository->getPrice() * $quantity;//je récupère le prix du produit dans la bdd que je multiplie par la quantité de ce produit pour avoir le prix total dans cette variable lineShoppingCart et je l'assigne à la variable price

        $lineShoppingCart->setPrice($price);// je fixe dans la variable lineShoppingCart le prix total du produit que j'ai récupéré dans la variable price
        
        $shoppingCart->setUser($user);//Je fixe le user concerné avec la méthode setUser dans le panier 
        $shoppingCart = addLineShoppingCart($lineShoppingCart);//Je rajoute cette nouvelle ligne au panier avec la méthode addLineShoppingCart en l'assignant à la variable shoppingCart
        
        
        $entityManager->persist($shoppingCart);// je dis à Doctrine que je veux (éventuellement) sauvegarder ce panier (pas encore de requête)
        $entityManager->flush();// Je demande à Doctrine d'éxecuter réellement la requête



        return $this->return('line_shopping_cart.html.twig', [

           'product'  => $product

        ]);
    }
}
