<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use App\Entity\ShoppingCart;
use App\Entity\Category;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/shopping_cart", name="shopping_cart")
     */
    
    public function showCart(Product $product)
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);


        return $this->render("shopping_cart.html.twig", [

            "product" => $product,
           
        ]);
    }

    /**
     * @Route("/shopping_cart/add/{product}", name="add_to_shopping_cart")
     */
    
    public function addToCart(Product $product)
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);

        $user = $this->getUser();

        return $this->render("add_to_cart.html.twig", [

            "product" => $product
           
        ]);
    }

}
