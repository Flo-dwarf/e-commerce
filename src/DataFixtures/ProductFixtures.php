<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Opinion;
use App\Entity\User;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 1; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence())->setProduct(0)->setPublished(true);
            $manager->persist($category);

            $content = join($faker->paragraphs(5));


            for ($j = 1; $j < mt_rand(4, 6); $j++) {
                $product = new Product();
                $product->setDesignation($faker->sentence())
                    ->setContent($content)
                    ->setImage($faker->imageUrl())
                    ->setPublishedDate($faker->dateTimeBetween('-6 months')) //new \DateTime()
                    ->addCategory($category)
                    ->setStock("10")
                    ->setPrice("10");

                $manager->persist($product);

                for ($k = 1; $k < mt_rand(4, 10); $k++) {
                    $comment = new Opinion();

                    $content = join($faker->paragraphs(2));

                    $now = new \DateTime();
                    $days = $now->diff($product->getPublishedDate())->days;
                    
                    $minimum = '-' . $days . ' days'; //-100days
                    $user = new User();
                    $user->setCity($faker->city)
                        ->setCountry($faker->country)
                        ->setEmail($faker->email)
                        ->setName($faker->name)
                        ->setPassword($faker->password)
                        ->setStreet($faker->streetName)
                        ->setUsername($faker->userName)
                        ->setZipCode($faker->streetSuffix);
                    $manager->persist($user);

                    $comment->setAuthor($user)
                            ->setContent($content)
                            ->setDate($faker->dateTimeBetween($minimum))
                            ->setProduct($product)
                            ->setStarEvaluation($faker->numberBetween(0, 5));

                    $manager->persist($comment);
                }
            }


        }
        $manager->flush();
    }
}
