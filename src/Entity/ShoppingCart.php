<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $articleNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineShoppingCart", mappedBy="ShoppingCart")
     */
    private $lineShoppingCarts;

    public function __construct()
    {
        $this->lineShoppingCarts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getArticleNumber(): ?int
    {
        return $this->articleNumber;
    }

    public function setArticleNumber(int $articleNumber): self
    {
        $this->articleNumber = $articleNumber;

        return $this;
    }

    public function getTotalPrice(): ?int
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(?int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function setProductId(int $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getTotalProducts(): ?int
    {
        return $this->totalProducts;
    }

    public function setTotalProducts(?int $totalProducts): self
    {
        $this->totalProducts = $totalProducts;

        return $this;
    }

    /**
     * @return Collection|LineShoppingCart[]
     */
    public function getLineShoppingCarts(): Collection
    {
        return $this->lineShoppingCarts;
    }

    public function addLineShoppingCart(LineShoppingCart $lineShoppingCart): self
    {
        if (!$this->lineShoppingCarts->contains($lineShoppingCart)) {
            $this->lineShoppingCarts[] = $lineShoppingCart;
            $lineShoppingCart->setShoppingCart($this);
        }

        return $this;
    }

    public function removeLineShoppingCart(LineShoppingCart $lineShoppingCart): self
    {
        if ($this->lineShoppingCarts->contains($lineShoppingCart)) {
            $this->lineShoppingCarts->removeElement($lineShoppingCart);
            // set the owning side to null (unless already changed)
            if ($lineShoppingCart->getShoppingCart() === $this) {
                $lineShoppingCart->setShoppingCart(null);
            }
        }

        return $this;
    }
}
